//
//  main.m
//  Expression
//
//  Created by Igor Santos on 12-09-24.
//  Copyright (c) 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IGSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IGSAppDelegate class]));
    }
}
