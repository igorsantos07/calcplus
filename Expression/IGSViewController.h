//
//  IGSViewController.h
//  Calc+
//
//  Created by Igor Santos on 12-09-24.
//  Copyleft (c) 2012. Some rights reserved under CC BY-SA 2.5.
//  More details in COPYING and README files.
//

#import <UIKit/UIKit.h>

@interface IGSViewController : UIViewController <UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UIView *viewCalculator;

@property (strong, nonatomic) IBOutlet UILabel *fieldMain;
@property (strong, nonatomic) IBOutlet UILabel *fieldOperation;
@property (strong, nonatomic) IBOutlet UILabel *fieldMemoryStatus;

@property (strong, nonatomic) IBOutlet UIButton *buttonMS;
@property (strong, nonatomic) IBOutlet UIButton *buttonMR;
@property (strong, nonatomic) IBOutlet UIButton *buttonMC;
@property (strong, nonatomic) IBOutlet UIButton *buttonSeparator;
@property (strong, nonatomic) IBOutlet UIButton *buttonRoot;

@property (nonatomic) int nextOperation;
@property (nonatomic) float lastValue;
@property (nonatomic) float memoryValue;
@property (nonatomic) bool memoryHasValue;
@property (nonatomic) bool clearField;

- (IBAction)buttonLongPressed:(id)sender;
- (IBAction)buttonPressed:(UIButton *)sender;
@end
