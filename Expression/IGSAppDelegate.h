//
//  IGSAppDelegate.h
//  Calc+
//
//  Created by Igor Santos on 12-09-24.
//  Copyleft (c) 2012. Some rights reserved under CC BY-SA 2.5.
//  More details in COPYING and README files.
//

#import <UIKit/UIKit.h>

@class IGSViewController;

@interface IGSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IGSViewController *viewController;

@end
