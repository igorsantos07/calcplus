//
//  IGSViewController.m
//  Calc+
//
//  Created by Igor Santos on 12-09-24.
//  Copyleft (c) 2012. Some rights reserved under CC BY-SA 2.5.
//  More details in COPYING and README files.
//

#import "IGSViewController.h"

@interface IGSViewController ()

@end

@implementation IGSViewController
@synthesize nextOperation;
@synthesize lastValue;
@synthesize clearField;

@synthesize memoryValue;
@synthesize memoryHasValue;

@synthesize viewCalculator;

@synthesize fieldMain;
@synthesize fieldOperation;
@synthesize fieldMemoryStatus;

@synthesize buttonMS;
@synthesize buttonMR;
@synthesize buttonMC;
@synthesize buttonSeparator;
@synthesize buttonRoot;

NSString * const SettingsKeyUsePoint = @"usePoint";
NSString * const SettingsKeyRootDegree = @"rootDegree";

/**
 * TODO no more digits to avoid scientific notation with %g formatter.
 * On the other hand, NSNumberFormatter gives us float garbage, what's even worse for UX.
 */
int maxFieldLength = 8;

int hardRootDegree = -1;
int const ROOT_CUSTOM = 1;
int const ROOT_BUTTON = -1;

int const BT_DECSEP = 10;
int const BT_OP_EQ  = 11;
int const BT_OP_DIV = 12;
int const BT_OP_MUL = 13;
int const BT_OP_SUB = 14;
int const BT_OP_ADD = 15;
int const BT_OP_PERC = 16;
int const BT_OP_POWE = 17;
int const BT_OP_ROOT = 18;
int const BT_BKSP = 19;
int const BT_UNDO = 20;
int const BT_CLEAR = 21;
int const BT_MS = 22;
int const BT_MR = 23;
int const BT_MC = 24;

NSString * const CHAR_EQ = @"=";
NSString * const CHAR_ADD = @"+";
NSString * const CHAR_SUB = @"−";
NSString * const CHAR_MUL = @"×";
NSString * const CHAR_DIV = @"÷";
NSString * const CHAR_POWE = @"^";
NSString * const CHAR_ROOT_2 = @"√";
NSString * const CHAR_ROOT_3 = @"∛";
NSString * const CHAR_ROOT_4 = @"∜";

UIColor * colorButtonDisabledText;
UIColor * colorButtonMR;
UIColor * colorButtonMC;

NSUserDefaults * settings;

UIActionSheet * sheetRoot;
UIActionSheet * sheetDecimal;


/************************************* BORING IOS STUFF *************************************/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    colorButtonDisabledText = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1.0];
    colorButtonMR = [UIColor colorWithRed:0.2 green:0.4 blue:0.0 alpha:1.0];
    colorButtonMC = [UIColor colorWithRed:0.8 green:0.2 blue:0.2 alpha:1.0];
    
    [self setMemoryButtonsStatus:memoryHasValue];
    
    settings = [NSUserDefaults standardUserDefaults];
    [self verifySettingsForButtons];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(settingsDidChange:)
                   name:NSUserDefaultsDidChangeNotification
                 object:nil];
    
    sheetRoot = [[UIActionSheet alloc]
                    initWithTitle:@"Calculate which root?"
                        delegate:self
                cancelButtonTitle:@"Cancel"
            destructiveButtonTitle:nil
                otherButtonTitles:@"Custom root", @"Square root", @"Cube root", @"4th root", @"5th root", nil];
    
    sheetDecimal = [[UIActionSheet alloc]
                    initWithTitle:@"Use what as decimal separator?"
                         delegate:self
                cancelButtonTitle:@"Cancel"
           destructiveButtonTitle:nil
                otherButtonTitles:@"Comma: 0,0", @"Point: 0.0", nil];
}

- (void)viewDidUnload {
    [self setFieldMain:nil];
    [self setFieldOperation:nil];
    [self setFieldOperation:nil];
    [self setButtonMS:nil];
    [self setButtonMR:nil];
    [self setButtonMC:nil];
    [self setFieldMemoryStatus:nil];
    [self setViewCalculator:nil];
    [self setButtonSeparator:nil];
    [self setButtonRoot:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //when implementing horizontal view, remove this line!
    return NO;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


/************************* ACTIONS TO BE CALLED WITH CERTAIN EVENTS *************************/

- (NSString *)separeDecimalWithComma:(bool)withComma number:(NSString *)number {
    NSString *dSep = withComma? @"," : @".";
    NSString *tSep = withComma? @"." : @",";
    
    return [[[number
       stringByReplacingOccurrencesOfString:dSep withString:@"x"]
       stringByReplacingOccurrencesOfString:tSep withString:dSep]
       stringByReplacingOccurrencesOfString:@"x" withString:tSep];
}

- (void)settingsDidChange:(NSNotification *)notification {
    [settings synchronize];
    [self verifySettingsForButtons];
    
    fieldMain.text = [self separeDecimalWithComma:![settings boolForKey:SettingsKeyUsePoint]
                                           number:fieldMain.text];
}

- (void)setTitle:(NSString *)title forButton:(UIButton *)button {
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateSelected];
    [button setTitle:title forState:UIControlStateHighlighted];
}

- (void)verifySettingsForButtons {
    NSString *decimalSeparator = [settings boolForKey:SettingsKeyUsePoint]? @"." : @",";
    [self setTitle:decimalSeparator forButton:buttonSeparator];
    
    NSString *rootString = [self getStringForRoot:[settings integerForKey:SettingsKeyRootDegree]];
    [self setTitle:rootString forButton:buttonRoot];
}

- (void)setMemoryButtonsStatus:(bool)hasValue {
    memoryHasValue = hasValue;
    if (hasValue) {
        buttonMR.enabled = buttonMC.enabled = true;
        [buttonMR setTitleColor:colorButtonMR forState:UIControlStateNormal];
        [buttonMC setTitleColor:colorButtonMC forState:UIControlStateNormal];
    }
    else {
        buttonMR.enabled = buttonMC.enabled = false;
        [buttonMR setTitleColor:colorButtonDisabledText forState:UIControlStateDisabled];
        [buttonMC setTitleColor:colorButtonDisabledText forState:UIControlStateDisabled];
    }
}


/*************************** INTERACTION WITH THE CALCULATOR VISOR ***************************/

- (NSString *) getStringForRoot:(int)degree {
    switch (degree) {
        case 3: return CHAR_ROOT_3;
        case 4: return CHAR_ROOT_4;
        case 2:
        default: return CHAR_ROOT_2;
    }
}

- (NSString *) getOperationString:(int)operationConstant {
    switch (operationConstant) {
        case BT_OP_ADD:  return CHAR_ADD;
        case BT_OP_SUB:  return CHAR_SUB;
        case BT_OP_MUL:  return CHAR_MUL;
        case BT_OP_DIV:  return CHAR_DIV;
        case BT_OP_POWE: return CHAR_POWE;
        case BT_OP_ROOT: return [self getStringForRoot:[settings integerForKey:SettingsKeyRootDegree]];
        default:      return @"";
    }
}

- (void)setMainFieldFromFloat:(float)value {
    //should change to NSDecimalNumber to fix all this hassle
//    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    
//    NSString *stringValue = [formatter stringFromNumber:[NSNumber numberWithFloat:value]];
    NSString *stringValue = [NSString stringWithFormat:@"%g", value];
    
    if (![settings boolForKey:SettingsKeyUsePoint])
        stringValue = [self separeDecimalWithComma:true number:stringValue];
    
    fieldMain.text = stringValue;
}

- (float)getFloatFromMainField {
    NSString *stringValue = fieldMain.text;
    
    if (![settings boolForKey:SettingsKeyUsePoint])
        stringValue = [self separeDecimalWithComma:false number:stringValue];
    
    return [stringValue floatValue];
}

- (void)appendCharToMainField:(NSString *)charAsString {
    if (fieldMain.text.length < maxFieldLength &&
        !(fieldMain.text.length+1 == maxFieldLength &&
          ([charAsString isEqualToString:@","] || [charAsString isEqualToString:@"."])
        )
    ) {
        fieldMain.text = [NSString stringWithFormat:@"%@%@", fieldMain.text, charAsString];
    }
}

- (void)shouldClearField:(bool)shouldClear {
    if (shouldClear) {
        lastValue = [self getFloatFromMainField];
        fieldMain.text = @"";
        clearField = false;
    }
}


/************************************ ACTUAL BUTTON ACTIONS ************************************/

- (void)doNum:(int)number {
    [self shouldClearField:clearField];
    NSString *numberAsString = [NSString stringWithFormat:@"%i", number];
    if ([fieldMain.text isEqualToString:@"0"]) {
        if (number != 0) fieldMain.text = numberAsString;
    }
    else {
        [self appendCharToMainField:numberAsString];
    }
}

- (void)doOperation:(int)button {
    float currentValue, result;
    currentValue = [self getFloatFromMainField];
    
    //overwriting some values when the Root button is pressed, since it already gives the second number
    if (button == BT_OP_ROOT) {
        nextOperation = BT_OP_ROOT;
        if (hardRootDegree == ROOT_BUTTON) {
            lastValue = currentValue;
            currentValue = [settings integerForKey:SettingsKeyRootDegree]+2;
        }
        else if (hardRootDegree == ROOT_CUSTOM) {
            nextOperation = 0;
        }
        else {
            lastValue = currentValue;
            currentValue = hardRootDegree;
            hardRootDegree = -1;
        }
        //if it's not -1 (button default) or >1 (sheet degree) let's go with the normal flow
    }
    
    if (nextOperation != 0) {
        switch (nextOperation) {
            case BT_OP_ADD:  result = lastValue + currentValue; break;
            case BT_OP_SUB:  result = lastValue - currentValue; break;
            case BT_OP_MUL:  result = lastValue * currentValue; break;
            case BT_OP_DIV:  result = lastValue / currentValue; break;
            case BT_OP_POWE: result = pow(lastValue,   currentValue); break;
            case BT_OP_ROOT: result = pow(lastValue, 1/currentValue); break; //http://ca3.php.net/manual/en/function.sqrt.php#50093
        }

        [self setMainFieldFromFloat:result];
    }
    
    if (button == BT_OP_EQ || (button == BT_OP_ROOT && hardRootDegree != ROOT_CUSTOM)) nextOperation = 0;
    else nextOperation = button;
    
    fieldOperation.text = [self getOperationString:nextOperation];
    clearField = true;
}

- (void)doPercent {
    [self setMainFieldFromFloat:([self getFloatFromMainField] / 100)];
}

- (void)doDecimalSeparator {
    [self shouldClearField:clearField];
    NSString *separator = [settings boolForKey:SettingsKeyUsePoint]? @"." : @",";
    
    NSRange havePoint = [fieldMain.text rangeOfString:separator];
    if (havePoint.location == NSNotFound) {
        if (fieldMain.text.length == 0)
            fieldMain.text = @"0.";
        else
            [self appendCharToMainField:separator];
    }
}

- (void)doClear {
    lastValue = nextOperation = 0;
    fieldMain.text = fieldOperation.text = @"";
}

- (void)doBackspace {
    fieldMain.text = [fieldMain.text substringToIndex:fieldMain.text.length-1];
}

- (void)doMemoryOperation:(int)button {
    if (button == BT_MS) {
        memoryValue = [self getFloatFromMainField];
        fieldMemoryStatus.text = [NSString stringWithFormat:@"%g [M]", memoryValue];
        fieldMain.text = @"";
        [self setMemoryButtonsStatus:true];
    }
    
    else if (button == BT_MR) {
        [self shouldClearField:true];
        if (memoryHasValue) [self setMainFieldFromFloat:memoryValue];
    }
    
    else if(button == BT_MC) {
        memoryValue = 0;
        fieldMemoryStatus.text = @"";
        [self setMemoryButtonsStatus:false];
    }
}



/**************************** INTERNAL BUTTON ACTIONS (UI EVENTS) ****************************/

- (IBAction)buttonPressed:(UIButton *)button {
    char buttonCode = button.tag;
    
    if (buttonCode >= 0 && buttonCode <= 9) [self doNum:buttonCode];
    else if (buttonCode == BT_DECSEP)       [self doDecimalSeparator];
    else if (buttonCode == BT_OP_PERC)      [self doPercent];
    else if (buttonCode == BT_CLEAR)        [self doClear];
    else if (buttonCode == BT_BKSP)         [self doBackspace];
    else if (buttonCode >= BT_OP_EQ && buttonCode <= BT_OP_ROOT)                [self doOperation:buttonCode];
    else if (buttonCode == BT_MS || buttonCode == BT_MR || buttonCode == BT_MC) [self doMemoryOperation:buttonCode];
    else NSLog(@"The user pressed an unknown button");
}

- (IBAction)buttonLongPressed:(UILongPressGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        switch (gesture.view.tag) {
            case BT_OP_ROOT:
                [sheetRoot showInView:self.view];
            break;
            
            case BT_DECSEP:
                [sheetDecimal showInView:self.view];
            break;
        }
    }
}

- (void)actionSheet:(UIActionSheet *)sheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == sheet.cancelButtonIndex) return;
    
    if (sheet == sheetRoot) {
        hardRootDegree = buttonIndex+1; //numeric degrees index starts at 1, with the square root
        [self doOperation:BT_OP_ROOT];
    }
    else if (sheet == sheetDecimal) {
        // index 0 is comma, what means NO usePoint
        [settings setBool:buttonIndex forKey:SettingsKeyUsePoint];
    }
}
@end
